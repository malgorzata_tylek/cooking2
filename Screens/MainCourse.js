import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class MainCourse extends React.Component {
  render() {
    return (
      <View style={styles.primaryScreen}>
        <Text>Danie główne</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  primaryScreen: {
    flex: 1,
    backgroundColor: '#FFF',
  }
});
