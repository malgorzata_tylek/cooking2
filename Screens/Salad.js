import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Salad extends React.Component {
  render() {
    return (
      <View style={styles.primaryScreen}>
        <Text>Sałatka</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  primaryScreen: {
    flex: 1,
    backgroundColor: '#FFF',
  }
});
