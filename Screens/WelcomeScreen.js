import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  DrawerLayoutAndroid,
  View,
  TouchableHighlight,
  Image
} from 'react-native';

export default class WelcomeScreen extends React.Component {
  constructor() {
    super();
    this.openDrawer = this.openDrawer.bind(this);
  }

  openDrawer() {
    this.drawer.openDrawer();
  }

  render() {
    var navView = (
      <View style={{flex: 1}}>
        <View style={styles.navMainView}>
          <Image
            style={styles.navImage}
            source={require('../assets/images/pot.png')}
          />
        </View>

        <View style={{flex:3, backgroundColor: '#FFFFFF', margin: 40}}>
          <TouchableHighlight
            style={styles.drawerButton}
            underlayColor='#FFCDD2'
            onPress={() => this.props.navigation.navigate('soup')} >
              <Text style={styles.drawerButtonText}>
                Zupa
              </Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.drawerButton}
            underlayColor='#FFCDD2'
            onPress={() => this.props.navigation.navigate('mainCourse')} >
            <Text style={styles.drawerButtonText}>
              Danie główne
            </Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.drawerButton}
            underlayColor='#FFCDD2'
            onPress={() => this.props.navigation.navigate('salad')} >
            <Text style={styles.drawerButtonText}>
              Sałatka
            </Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.drawerButton}
            underlayColor='#FFCDD2'
            onPress={() => this.props.navigation.navigate('dessert')} >
            <Text style={styles.drawerButtonText}>
              Deser
            </Text>
          </TouchableHighlight>
        </View>
      </View>
    );
    return (
      <DrawerLayoutAndroid
        ref={_drawer => (this.drawer = _drawer)}
        drawerWidth={340}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={ () => navView}>

        <View style={styles.welcomeScreen}>
          <Text style={styles.primaryText}>MobileCook</Text>
          <Text style={styles.secondaryText}>Secondary text</Text>
          <TouchableHighlight
            style={styles.ghostButtonContainer}
            underlayColor='#D32F2F'
            onPress={this.openDrawer}
            >
            <Text style={styles.ghostButton}>
              Gotuj!
            </Text>
          </TouchableHighlight>
        </View>
      </DrawerLayoutAndroid>
    );
  }
}

const styles = StyleSheet.create({
  welcomeScreen: {
    flex: 1,
    backgroundColor: '#FF5252',
    alignItems: 'center',
    justifyContent: 'center'
  },

  navMainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#D32F2F'
  },

  navImage: {
    width: 100,
    height: 100
  },

  ghostButtonContainer: {
    width: 200,
    marginTop: 10,
    padding: 8,
    borderRadius: 2,
    borderWidth: 1,
    borderColor: '#FFF'
  },

  ghostButton: {
    color: '#FFF',
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontWeight: '100',
    fontSize: 24
  },

  drawerButton: {
    marginTop: 10,
    padding: 12,
    borderRadius: 2,
    borderWidth: 2,
    borderColor: '#FF5252',
    justifyContent: 'center'
  },

  drawerButtonText: {
    color: '#FF5252',
    fontFamily: 'Roboto',
    fontWeight: '100',
    fontSize: 18
  },

  primaryText: {
    color: '#FFFFFF',
    fontFamily: 'Roboto',
    fontWeight: '400',
    fontSize: 36
  },

  secondaryText: {
    color: '#FFFFFF',
    fontFamily: 'Roboto',
    fontWeight: '100',
    fontSize: 18
  }
});
