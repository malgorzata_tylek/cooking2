import React from 'react';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import WelcomeScreen from './Screens/WelcomeScreen';
import Soup from './Screens/Soup';
import MainCourse from './Screens/MainCourse';
import Salad from './Screens/Salad';
import Dessert from './Screens/Dessert';
import { Text } from 'react-native';

const CookingApp = StackNavigator({
  homeStack: { screen: WelcomeScreen },
  soup: { screen: Soup },
  mainCourse: { screen: MainCourse },
  salad: { screen: Salad },
  dessert: { screen: Dessert }
}, {
  // Default config for all screens
  headerMode: 'none',
  title: 'Main',
  initialRouteName: 'homeStack'
})

export default CookingApp;
